\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Literature Review}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Long-range Interaction Driven Mesoscale Assembly}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Factors That Control Long-range Interactions}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Characterization and Prediction of Electronic Structure, Optical Properties, and Long-range Interactions}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Promising Cylindrical and Planar Units for Mesoscale Self-assembly}{7}{subsection.2.4}
\contentsline {section}{\numberline {3}Research Plan}{10}{section.3}
\contentsline {section}{\numberline {4}Methods}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Full-spectral Electronic and Optical Characterization}{12}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Specific Regional-spectral Optical Characterization}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Direct Determination of Long-range Interactions}{14}{subsection.4.3}
\contentsline {section}{\numberline {5}Preliminary Work}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}Optical Properties and Long-range Interactions of Cylindrical Biomolecule: DNA}{16}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Optical Properties and Long-range Interactions of Planar Inorganic Material: Mica}{19}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Photon Management Assembly Constructed from Cylindrical Biomolecules}{21}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Research Steps}{21}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Preliminary work}{24}{subsubsection.5.3.2}
\contentsline {section}{\numberline {6}Conclusion/Discussion/Summary}{28}{section.6}
