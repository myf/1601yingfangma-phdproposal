\begin{thebibliography}{10}

\bibitem{laird_influence_2006}
David~A. Laird.
\newblock Influence of layer charge on swelling of smectites.
\newblock {\em Applied Clay Science}, 34(1–4):74--87, October 2006.

\bibitem{dryden_long-range_2014}
Daniel~M. Dryden.
\newblock Long-range interactions in biomolecular-inorganic assemblies.
\newblock Master's thesis, Case Western Reserve University, 2014.

\bibitem{Benjamin_2016}
Benjamin Gilbert, Luis~R. Comolli, Ruth~M. Tinnacher, Martin Kunz, and
  Jillian~F. Banfield.
\newblock Formation and restacking of disordered smectite osmotic hydrates.
\newblock {\em In preparation}, 2016.

\bibitem{srivastava_light-controlled_2010}
S.~Srivastava, A.~Santos, K.~Critchley, K.-S. Kim, P.~Podsiadlo, K.~Sun,
  J.~Lee, C.~Xu, G.~D. Lilly, S.~C. Glotzer, and N.~A. Kotov.
\newblock Light-{Controlled} {Self}-{Assembly} of {Semiconductor}
  {Nanoparticles} into {Twisted} {Ribbons}.
\newblock {\em Science}, 327(5971):1355--1359, March 2010.

\bibitem{grzelczak_directed_2010}
Marek Grzelczak, Jan Vermant, Eric~M. Furst, and Luis~M. Liz-Marzán.
\newblock Directed {Self}-{Assembly} of {Nanoparticles}.
\newblock {\em ACS Nano}, 4(7):3591--3605, July 2010.

\bibitem{ma_determination_2014}
Yingfang Ma, Diana~M. Acosta, Jon~R. Whitney, Rudolf Podgornik, Nicole~F.
  Steinmetz, Roger~H. French, and V.~Adrian Parsegian.
\newblock Determination of the second virial coefficient of bovine serum
  albumin under varying {pH} and ionic strength by composition-gradient
  multi-angle static light scattering.
\newblock {\em Journal of Biological Physics}, 41(1):85--97, November 2014.

\bibitem{sahin_comparative_2010}
Erinc Sahin, Adeola~O. Grillo, Melissa~D. Perkins, and Christopher~J. Roberts.
\newblock Comparative effects of {pH} and ionic strength on protein–protein
  interactions, unfolding, and aggregation for {IgG}1 antibodies.
\newblock {\em Journal of Pharmaceutical Sciences}, 99(12):4830--4848, December
  2010.

\bibitem{steinmetz_pegylated_2009}
N.~F. Steinmetz and M.~Manchester.
\newblock {PEGylated} viral nanoparticles for biomedicine: the impact of {PEG}
  chain length on {VNP} cell interactions in vitro and ex vivo.
\newblock {\em Biomacromolecules}, 10(4):784--792, 2009.

\bibitem{ma_long-range_}
Yingfang Ma.
\newblock Long-range {Interactions} and {Second} {Virial} {Coefficients} of
  {Biomolecular} {Materials}.
\newblock Master's thesis, Case Western Reserve University.

\bibitem{glotzer_self-assembly:_2004}
S.~C. Glotzer, M.~J. Solomon, and N.~A. Kotov.
\newblock Self-assembly: {From} nanoscale to microscale colloids.
\newblock {\em AIChE Journal}, 50(12):2978--2985, December 2004.

\bibitem{french_origins_2000}
RH~French.
\newblock Origins and applications of {London} dispersion forces and {Hamaker}
  constants in ceramics.
\newblock {\em Journal of the American Ceramic Society}, 83(9):2117--2146,
  September 2000.

\bibitem{rajter_van_2007}
Rick~F. Rajter, Rudi Podgornik, V.~Adrian Parsegian, Roger~H. French, and W.~Y.
  Ching.
\newblock van der {Waals}-{London} dispersion interactions for optically
  anisotropic cylinders: {Metallic} and semiconducting single-wall carbon
  nanotubes.
\newblock {\em Phys. Rev. B}, 76(4):045417, July 2007.

\bibitem{woollam_overview_1999}
John~A. Woollam, Blaine~D. Johs, Craig~M. Herzinger, James~N. Hilfiker, Ron~A.
  Synowicki, and Corey~L. Bungay.
\newblock Overview of variable-angle spectroscopic ellipsometry ({VASE}): {I}.
  {Basic} theory and typical applications.
\newblock volume~72, pages 3--28, July 1999.

\bibitem{laref_electronic_2008}
A.~Laref and S.~Laref.
\newblock Electronic and optical properties of {SiC} polytypes using a
  transferable semi-empirical tight-binding model.
\newblock {\em physica status solidi (b)}, 245(1):89--100, January 2008.

\bibitem{ching_theoretical_1990}
W.~Y. Ching.
\newblock Theoretical {Studies} of the {Electronic} {Properties} of {Ceramic}
  {Materials}.
\newblock {\em Journal of the American Ceramic Society}, 73(11):3135--3160,
  November 1990.

\bibitem{parsegian_van_2006}
V.~Adrian Parsegian.
\newblock {\em Van der {Waals} forces: a handbook for biologists, chemists,
  engineers, and physicists}.
\newblock Cambridge University Press, 2006.

\bibitem{dryden_van_2015}
Daniel~M. Dryden, Jaime~C. Hopkins, Lin~K. Denoyer, Lokendra Poudel, Nicole~F.
  Steinmetz, Wai-Yim Ching, Rudolf Podgornik, Adrian Parsegian, and Roger~H.
  French.
\newblock van der {Waals} {Interactions} on the {Mesoscale}: {Open}-{Science}
  {Implementation}, {Anisotropy}, {Retardation}, and {Solvent} {Effects}.
\newblock {\em Langmuir}, 31(37):10145--10153, September 2015.

\bibitem{some_light-scattering-based_2013}
Daniel Some.
\newblock Light-scattering-based analysis of biomolecular interactions.
\newblock {\em Biophysical Reviews}, 5(2):147--158, June 2013.

\bibitem{wu_osmotic_1999}
Jianzhong Wu and John~M. Prausnitz.
\newblock Osmotic pressures of aqueous bovine serum albumin solutions at high
  ionic strength.
\newblock {\em Fluid Phase Equilibria}, 155(1):139--154, February 1999.

\bibitem{roth_van_1996}
C~M Roth, B~L Neal, and A~M Lenhoff.
\newblock Van der waals interactions involving proteins.
\newblock {\em Biophysical Journal}, 70(2):977--987, February 1996.

\bibitem{arzensek_colloidal_2012}
Dejan Arzensek, Drago Kuzman, and Rudolf Podgornik.
\newblock Colloidal interactions between monoclonal antibodies in aqueous
  solutions.
\newblock {\em Journal of colloid and interface science}, 384(1):207--216,
  October 2012.

\bibitem{RayleighScattering}
\url{http://www.invocom.et.put.poznan.pl/~invocom/C/P1-9/swiatlowody_en/p1-1_2_2.htm},
  2014.
\newblock [Online; accessed 12-September-2014].

\bibitem{zhang_simulations_2007}
{Zhang}, {Tang}, Nicholas~A. Kotov, and Sharon~C. Glotzer.
\newblock Simulations and {Analysis} of {Self}-{Assembly} of {CdTe}
  {Nanoparticles} into {Wires} and {Sheets}.
\newblock {\em Nano Letters}, 7(6):1670--1675, June 2007.

\bibitem{lan_dna-programmed_2014}
Xiang Lan and Qiangbin Wang.
\newblock {DNA}-programmed self-assembly of photonic nanoarchitectures.
\newblock {\em NPG Asia Materials}, 6(4):e97, April 2014.

\bibitem{schimelman_optical_2015}
Jacob~B. Schimelman, Daniel~M. Dryden, Lokendra Poudel, Katherine~E. Krawiec,
  Yingfang Ma, Rudolf Podgornik, V.~Adrian Parsegian, Linda~K. Denoyer, Wai-Yim
  Ching, Nicole~F. Steinmetz, and Roger~H. French.
\newblock Optical properties and electronic transitions of {DNA}
  oligonucleotides as a function of composition and stacking sequence.
\newblock {\em Physical Chemistry Chemical Physics}, 17(6):4589--4599, January
  2015.

\bibitem{poudel_electronic_2014}
Lokendra Poudel, Paul Rulis, Lei Liang, and W.~Y. Ching.
\newblock Electronic structure, stacking energy, partial charge, and hydrogen
  bonding in four periodic {B}-{DNA} models.
\newblock {\em Physical Review E}, 90(2):022705, August 2014.

\bibitem{stigter_theory_1993}
Dirk Stigter and Ken~A. Dill.
\newblock Theory for second virial coefficients of short {DNA}.
\newblock {\em The Journal of Physical Chemistry}, 97(49):12995--12997,
  December 1993.

\bibitem{nicolai_ionic_1989}
Taco Nicolai and Michel Mandel.
\newblock The ionic strength dependence of the second virial coefficient of low
  molar mass {DNA} fragments in aqueous solutions.
\newblock {\em Macromolecules}, 22(1):438--444, January 1989.

\bibitem{borochov_dependence_1981}
Nina Borochov, Henryk Eisenberg, and Zvi Kam.
\newblock Dependence of {DNA} conformation on the concentration of salt.
\newblock {\em Biopolymers}, 20(1):231--235, January 1981.

\bibitem{program_english:_2008}
U.~S. Department of Energy Human~Genome Program.
\newblock English: {Telomere} caps, January 2008.

\bibitem{ambrus_human_2006}
Attila Ambrus, Ding Chen, Jixun Dai, Tiffanie Bialis, Roger~A. Jones, and
  Danzhou Yang.
\newblock Human telomeric sequence forms a hybrid-type intramolecular
  {G}-quadruplex structure with mixed parallel/antiparallel strands in
  potassium solution.
\newblock {\em Nucleic Acids Research}, 34(9):2723--2735, 2006.

\bibitem{_g-quadruplex_2016}
G-quadruplex, January 2016.
\newblock Page Version ID: 699699896.

\bibitem{Lokendra_2016}
Lokendra Poudel, Nicole~F. Steinmetz, Roger~H. French, V.~Adrian Parsegian,
  Rudolf Podgornik, and Wai-Yim Ching.
\newblock Metal ions, solvent effects, hydrogen bonding, electronic structure
  and topology in human telomeric g-quadruplex dna.
\newblock {\em In preparation}, 2016.

\bibitem{xu_new_2006}
Yan Xu, Yuki Noguchi, and Hiroshi Sugiyama.
\newblock The new models of the human telomere d[{AGGG}({TTAGGG})3] in {K}+
  solution.
\newblock {\em Bioorganic \& Medicinal Chemistry}, 14(16):5584--5591, August
  2006.

\bibitem{liu_tin-coated_2013}
Yihang Liu, Yunhua Xu, Yujie Zhu, James~N. Culver, Cynthia~A. Lundgren, Kang
  Xu, and Chunsheng Wang.
\newblock Tin-{Coated} {Viral} {Nanoforests} as {Sodium}-{Ion} {Battery}
  {Anodes}.
\newblock {\em ACS Nano}, 7(4):3627--3634, April 2013.

\bibitem{chiang_biological_2012}
Chia-Ying Chiang, Jillian Epstein, Adam Brown, Jeremy~N. Munday, James~N.
  Culver, and Sheryl Ehrman.
\newblock Biological {Templates} for {Antireflective} {Current} {Collectors}
  for {Photoelectrochemical} {Cell} {Applications}.
\newblock {\em Nano Letters}, 12(11):6005--6011, November 2012.

\bibitem{lee_virus-templated_2012}
Youjin Lee, Junhyung Kim, Dong~Soo Yun, Yoon~Sung Nam, Yang Shao-Horn, and
  Angela~M. Belcher.
\newblock Virus-templated {Au} and {Au}–{Pt} core–shell nanowires and their
  electrocatalytic activities for fuel cell applications.
\newblock {\em Energy \& Environmental Science}, 5(8):8328, 2012.

\bibitem{tseng_digital_2006}
Ricky~J. Tseng, Chunglin Tsai, Liping Ma, Jianyong Ouyang, Cengiz~S. Ozkan, and
  Yang Yang.
\newblock Digital memory device based on tobacco mosaic virus conjugated with
  nanoparticles.
\newblock {\em Nature Nanotechnology}, 1(1):72--77, October 2006.

\bibitem{portney_nanoscale_2008}
Nathaniel~G. Portney, Alfredo~A. Martinez-Morales, and Mihrimah Ozkan.
\newblock Nanoscale {Memory} {Characterization} of {Virus}-{Templated}
  {Semiconducting} {Quantum} {Dots}.
\newblock {\em ACS Nano}, 2(2):191--196, February 2008.

\bibitem{TMVpic}
\url{http://www.cmb.wisc.edu/virology}.
\newblock [Online; accessed 18-February-2016].

\bibitem{niu_assembly_2007}
Zhongwei Niu, Michael~A. Bruckman, Siqi Li, L.~Andrew Lee, Byeongdu Lee,
  Sai~Venkatesh Pingali, P.~Thiyagarajan, and Qian Wang.
\newblock Assembly of {Tobacco} {Mosaic} {Virus} into {Fibrous} and
  {Macroscopic} {Bundled} {Arrays} {Mediated} by {Surface} {Aniline}
  {Polymerization}.
\newblock {\em Langmuir}, 23(12):6719--6724, June 2007.

\bibitem{bortz_optical_1989}
M.~L. Bortz and R.~H. French.
\newblock Optical reflectivity measurements using a laser plasma light source.
\newblock {\em Applied Physics Letters}, 55(19):1955--1957, November 1989.

\bibitem{_nucleoside_2016}
Nucleoside, February 2016.
\newblock Page Version ID: 704579265.

\bibitem{tan_optical_2003}
Guo-Long Tan, Michael~F. Lemon, and Roger~H. French.
\newblock Optical {Properties} and {London} {Dispersion} {Forces} of
  {Amorphous} {Silica} {Determined} by {Vacuum} {Ultraviolet} {Spectroscopy}
  and {Spectroscopic} {Ellipsometry}.
\newblock {\em Journal of the American Ceramic Society}, 86(11):1885--1892,
  November 2003.

\bibitem{french_optical_1998}
Roger~H. French, Harald Müllejans, and David~J. Jones.
\newblock Optical {Properties} of {Aluminum} {Oxide}: {Determined} from
  {Vacuum} {Ultraviolet} and {Electron} {Energy}-{Loss} {Spectroscopies}.
\newblock {\em Journal of the American Ceramic Society}, 81(10):2549--2557,
  October 1998.

\bibitem{dogic_ordered_2006}
Zvonimir Dogic and Seth Fraden.
\newblock Ordered phases of filamentous viruses.
\newblock {\em Current Opinion in Colloid \& Interface Science}, 11(1):47--55,
  April 2006.

\bibitem{kanduc_role_2009}
M.~Kanduč, A.~Naji, Y.~S. Jho, P.~A. Pincus, and R.~Podgornik.
\newblock The role of multipoles in counterion-mediated interactions between
  charged surfaces: strong and weak coupling.
\newblock {\em Journal of Physics: Condensed Matter}, 21(42):424103, 2009.

\end{thebibliography}
